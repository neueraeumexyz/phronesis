use_osc "127.0.0.1", 4560
use_debug true
use_real_time


define :midiMapmuscle do |mIn, mOut, v|
  if (v > 4000)
    v = 4000
  end
  
  dOut = mOut[1] - mOut[0]
  dIn = mIn[1] - mIn[0]
  dOut / dIn * (v - mIn[0]) + mOut[0]
end

live_loop :mappingmusclearm do
  use_real_time
  siga = sync "/osc:127.0.0.1:*/muscle/arm/sig"
  a = siga[0]
  puts a
  mIn =  [0.0, 4000.0]
  mOut = [0.0, 127.0]
  set :arm, midiMapmuscle(mIn, mOut, a)
  puts (0+get[:arm])
end

live_loop :mappingmuscleleg do
  use_real_time
  sigl = sync "/osc:127.0.0.1:*/muscle/leg/sig"
  l = sigl[0]
  puts l
  mIn =  [0.0, 4000.0]
  mOut = [0.0, 127.0]
  set :leg, midiMapmuscle(mIn, mOut, l)
  puts (0+get[:leg])
end

define :midiMapmean do |aIn, aOut, v|
  if (v < 100)
    v = 100
  end
  if (v > 2500)
    v = 2500
  end
  
  dOut = aOut[1] - aOut[0]
  dIn = aIn[1] - aIn[0]
  dOut / dIn * (v - aIn[0]) + aOut[0]
end

define :midiMapvari do |pIn, pOut, x|
  if (x < 100)
    x = 100
  end
  if (x > 2500)
    x = 2500
  end
  
  tOut = pOut[1] - pOut[0]
  tIn = pIn[1] - pIn[0]
  tOut / tIn * (x - pIn[0]) + pOut[0]
end

live_loop :mappingatem1 do
  mean = sync "/osc:127.0.0.1:*/breath/mean"#geglättete Daten (Tiefpassfilter)
  a = mean[0]
  puts "mean is", a
  aIn =  [1000.0, 2700.0]
  aOut = [0.0, 127.0]
  set :mean, midiMapmean(aIn, aOut, a)
  puts (0+get[:mean])
end

live_loop :mappingatem2 do
  var = sync "/osc:127.0.0.1:*/breath/variance"
  vari = var[0]
  puts "variance is", vari
  pIn =  [500.0, 2000.0]
  pOut = [0.0, 127.0]
  set :vari, midiMapvari(pIn, pOut, vari)
  puts (0+get[:vari])
end

live_loop :mappingatem3 do
  relative = sync "/osc:127.0.0.1:*/breath/map"
  c = relative[0]
  puts "map is", c
  exhale = sync "/osc:127.0.0.1:*/breath/exhale"
  puts exhale[0]
  e = exhale[0]
  puts "exhale is", e
  end
  
  