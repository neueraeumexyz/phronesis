#include <Arduino.h>
#include <FS.h> //this needs to be first, or it all crashes and burns...
#include <SPI.h>
#include <Wire.h>
#include <WiFi.h>
#include <WebServer.h>
#include <DNSServer.h>
#include <AsyncJson.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <ArduinoJson.h>
#include <ArduinoOSCWiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ArduinoJson.h>
// #include <LittleFS.h>
#include <SPIFFS.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <ArduinoJson.h>
#include <esp_err.h>
#include <esp_wifi.h>

// #define LED_BUILTIN 5
#define BUTTON 0
#define SDA 18
#define SCL 16

#define OSC_IMU_FPS 20
#define OSC_CALIB_FPS 1

// WebServer
AsyncWebServer server(80);
DNSServer dns;
File fsUploadFile;

// WiFi
AsyncWiFiManager wifiManager(&server, &dns);

#define CONFIG_VERSION "opt1"
#define STRING_LEN 128

// config
const char *calibFile = "/calib.json";
const char *configFile = "/config.json";
String sensorName = "movement-" + WiFi.macAddress();
int osc_port = 5460;
String osc_host = "192.168.100.255";

// sensors
Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28, &Wire);
uint8_t bnoSystem = 0, bnoAccel = 0, bnoGyro = 0, bnoMag = 0;

TaskHandle_t pTaskWiFi;

// Declarations
void taskWiFi(void *parameter);
void setupOSC();
void readSensor();
void printSensorDataJson(const sensors_event_t &event, const imu::Quaternion &quat);
void displaySensorDetails(void);
void displaySensorStatus(void);
void displayCalStatus(void);
bool saveCalib();
bool loadCalib();
bool saveConfig();
bool loadConfig();
void printFileSystem();

void setup()
{
  // serial
  Serial.begin(115200);

  // GPIO
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  pinMode(BUTTON, INPUT_PULLUP);

  // setup BNO055
  Wire.begin(SDA, SCL);
  while (!bno.begin())
  {
    /* There was a problem detecting the BNO055 ... check your connections */
    Serial.println("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
    delay(5000);
    break;
  }
  bno.setExtCrystalUse(true);

  delay(1000);

  // init file system
  if (!SPIFFS.begin(true))
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
  }
  printFileSystem();

  // load calib
  loadCalib();

  // get calib state
  bno.getCalibration(&bnoSystem, &bnoGyro, &bnoAccel, &bnoMag);

  /* Display some basic information on this sensor */
  displaySensorDetails();
  displaySensorStatus();
  displayCalStatus();

  // load config
  sensorName.replace(':', '-');
  loadConfig();

  // setup WiFI task
  xTaskCreatePinnedToCore(taskWiFi,                      /* Function to implement the task */
                          "taskButton",                  /* Name of the task */
                          getArduinoLoopTaskStackSize(), /* Stack size in words */
                          NULL,                          /* Task input parameter */
                          1,                             /* Priority of the task */
                          &pTaskWiFi,                    /* Task handle. */
                          1                              /* Core where the task should run */
  );
}

void loop()
{
  // LED shows Calib or WiFi state
  // if (!bno.isFullyCalibrated())
  // {
  //   digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  // }
  // else
  // {
  //   digitalWrite(LED_BUILTIN, WiFi.isConnected());
  // }

  // get calib state
  uint8_t system, gyro, accel, mag;
  system = gyro = accel = mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);

  bool betterCalib = false;
  if (system != bnoSystem || gyro != bnoGyro || accel != bnoAccel || mag != bnoMag)
  {
    displayCalStatus();
    bnoSystem = system;
    bnoGyro = gyro;
    bnoAccel = accel;
    bnoMag = mag;
  }

  // button -> calib
  if (digitalRead(BUTTON) == LOW)
  {

    if (bno.isFullyCalibrated())
    {
      Serial.println("Save config");
      saveCalib();
    }
    else
    {
      Serial.println("Sensor is not calibrated yet!");
    }
  }

  digitalWrite(LED_BUILTIN, WiFi.isConnected());
  readSensor();

  delay(10);
}

void taskWiFi(void *parameter)
{

  // setup WiFi
  if (digitalRead(BUTTON) == LOW)
  {
    Serial.println("Resetting WiFi");
    WiFi.disconnect(true, true);
    esp_wifi_restore();
  }

  wifiManager.setConnectTimeout(10);
  // connect to WiFi
  if (!wifiManager.autoConnect(sensorName.c_str()))
  {
    Serial.println("failed to connect, we should reset as see if it connects");
  }
  // WiFi.begin("Playour", "HandankerFeldwegProst");
  // WiFi.softAP(sensorName.c_str());
  delay(1000);

  // setup mDNS
  if (!MDNS.begin(sensorName.c_str()))
  {
    Serial.println("Error setting up MDNS responder!");
  }
  else
  {
    Serial.printf("mDNS responder started with hostname %s.local\n", sensorName.c_str());
  }

  Serial.println(WiFi.localIP());
  Serial.println(WiFi.broadcastIP());
  Serial.println(WiFi.softAPIP());

  // HTTP Server
  server.begin();
  server.on("/", [](AsyncWebServerRequest *request)
            {
    // get updated config
    bool changedConfig = false;
    int params = request->params();
    for(int i=0;i<params;i++){
      AsyncWebParameter* p = request->getParam(i);
      if(p->isPost()){
        Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        auto name = p->name();
        auto value = p->value();

        if(name.equals("name") && value != sensorName){
          sensorName = String(value);
          changedConfig = true;
        } else if(name.equals("osc_host") && value != osc_host){
          osc_host = String(value);
          changedConfig = true;
        } else if(name.equals("osc_port") && value.toInt() != osc_port){
          osc_port = value.toInt();
          changedConfig = true;
        }
      }
    }

    // save config if changed
    if(changedConfig){
      saveConfig();
    }

    String form = String(
      "<html><head><title>Setup</title></head>"
      "<body>"
      "  <form action='/' method='POST'>"
      "   <label>Device name: </label><input type='text' name='name' value='") + sensorName + String("'><br>"
      "   <label>OSC Server: </label><input type='text' name='osc_host' value='") + osc_host + String("'><br>"
      "   <label>OSC Port: </label><input type='text' name='osc_port' value='") + osc_port + String("'><br>"
      "   <input type='submit' value='Update'>"
      "  </form>"
      "</body>"
      "</html>"
    );
    request->send(200, "text/html", form); });

  // setup OSC
  Serial.println("Setup OSC...");
  setupOSC();
}

void setupOSC()
{
  if (!WiFi.isConnected())
  {
    return;
  }

  // IMU
  sensors_event_t event;
  bno.getEvent(&event);

  // Quaternion
  imu::Quaternion quat = bno.getQuat();

  // calib state
  uint8_t sys, gyro, accel, mag = 0;
  bno.getCalibration(&sys, &gyro, &accel, &mag);

  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/gyro/x", event.gyro.x)->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/gyro/y", event.gyro.y)->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/gyro/z", event.gyro.z)->setFrameRate(OSC_IMU_FPS);

  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/acc/x", event.acceleration.x)->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/acc/y", event.acceleration.y)->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/acc/z", event.acceleration.z)->setFrameRate(OSC_IMU_FPS);

  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/orientatíon/roll", event.orientation.roll)->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/orientatíon/pitch", event.orientation.pitch)->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/orientatíon/yaw", event.orientation.heading)->setFrameRate(OSC_IMU_FPS);

  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/x", quat.x())->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/y", quat.y())->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/z", quat.z())->setFrameRate(OSC_IMU_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/w", quat.w())->setFrameRate(OSC_IMU_FPS);

  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/sys", sys)->setFrameRate(OSC_CALIB_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/gyro", gyro)->setFrameRate(OSC_CALIB_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/accel", accel)->setFrameRate(OSC_CALIB_FPS);
  OscWiFi.publish(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/mag", mag)->setFrameRate(OSC_CALIB_FPS);
}

void readSensor()
{
  // Get a new sensor event
  sensors_event_t event;
  bno.getEvent(&event);

  // absolute position in quaternion
  imu::Quaternion quat = bno.getQuat();

  // calib state
  uint8_t sys, gyro, accel, mag = 0;
  bno.getCalibration(&sys, &gyro, &accel, &mag);

  if (WiFi.isConnected())
  {
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/gyro/x", event.gyro.x);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/gyro/y", event.gyro.y);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/gyro/z", event.gyro.z);

    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/acc/x", event.acceleration.x);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/acc/y", event.acceleration.y);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/acc/z", event.acceleration.z);

    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/orientatíon/roll", event.orientation.roll);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/orientatíon/pitch", event.orientation.pitch);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/orientatíon/yaw", event.orientation.heading);

    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/x", quat.x());
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/y", quat.y());
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/z", quat.z());
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/quat/w", quat.w());

    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/sys", sys);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/gyro", gyro);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/accel", accel);
    OscWiFi.send(osc_host, osc_port, "/movement/" + sensorName + "/imu/calib/mag", mag);
  }

  printSensorDataJson(event, quat);

  // /* The WebSerial 3D Model Viewer expects data as heading, pitch, roll */
  // Serial.print(F("Orientation: "));
  // Serial.print(360 - (float)event.orientation.x);
  // Serial.print(F(", "));
  // Serial.print((float)event.orientation.y);
  // Serial.print(F(", "));
  // Serial.print((float)event.orientation.z);
  // Serial.println(F(""));

  // /* The WebSerial 3D Model Viewer also expects data as roll, pitch, heading */
  // Serial.print(F("Quaternion: "));
  // Serial.print((float)quat.w(), 4);
  // Serial.print(F(", "));
  // Serial.print((float)quat.x(), 4);
  // Serial.print(F(", "));
  // Serial.print((float)quat.y(), 4);
  // Serial.print(F(", "));
  // Serial.print((float)quat.z(), 4);
  // Serial.println(F(""));

  // /* Also send calibration data for each sensor. */
  // Serial.print(F("Calibration: "));
  // Serial.print(sys, DEC);
  // Serial.print(F(", "));
  // Serial.print(gyro, DEC);
  // Serial.print(F(", "));
  // Serial.print(accel, DEC);
  // Serial.print(F(", "));
  // Serial.print(mag, DEC);
  // Serial.println(F(""));
}

void printSensorDataJson(const sensors_event_t &event, const imu::Quaternion &quat)
{
  StaticJsonDocument<512> doc;

  doc["sensor"]["name"] = sensorName;

  doc["imu"]["gyro"]["x"] = event.gyro.x;
  doc["imu"]["gyro"]["y"] = event.gyro.y;
  doc["imu"]["gyro"]["z"] = event.gyro.z;

  doc["imu"]["acc"]["x"] = event.acceleration.x;
  doc["imu"]["acc"]["y"] = event.acceleration.y;
  doc["imu"]["acc"]["z"] = event.acceleration.z;

  doc["imu"]["orientation"]["yaw"] = event.orientation.heading;
  doc["imu"]["orientation"]["pitch"] = event.orientation.pitch;
  doc["imu"]["orientation"]["roll"] = event.orientation.roll;

  doc["imu"]["quat"]["x"] = quat.x();
  doc["imu"]["quat"]["y"] = quat.y();
  doc["imu"]["quat"]["z"] = quat.z();
  doc["imu"]["quat"]["w"] = quat.w();

  serializeJson(doc, Serial);
  Serial.println();
}

/**************************************************************************/
/*
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/
void displaySensorDetails(void)
{
  sensor_t sensor;
  bno.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print("Sensor:       ");
  Serial.println(sensor.name);
  Serial.print("Driver Ver:   ");
  Serial.println(sensor.version);
  Serial.print("Unique ID:    ");
  Serial.println(sensor.sensor_id);
  Serial.print("Max Value:    ");
  Serial.print(sensor.max_value);
  Serial.println(" xxx");
  Serial.print("Min Value:    ");
  Serial.print(sensor.min_value);
  Serial.println(" xxx");
  Serial.print("Resolution:   ");
  Serial.print(sensor.resolution);
  Serial.println(" xxx");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}

/**************************************************************************/
/*
    Display some basic info about the sensor status
*/
/**************************************************************************/
void displaySensorStatus(void)
{
  /* Get the system status values (mostly for debugging purposes) */
  uint8_t system_status, self_test_results, system_error;
  system_status = self_test_results = system_error = 0;
  bno.getSystemStatus(&system_status, &self_test_results, &system_error);

  /* Display the results in the Serial Monitor */
  Serial.println("");
  Serial.print("System Status: 0x");
  Serial.println(system_status, HEX);
  Serial.print("Self Test:     0x");
  Serial.println(self_test_results, HEX);
  Serial.print("System Error:  0x");
  Serial.println(system_error, HEX);
  Serial.println("");
  delay(500);
}

/**************************************************************************/
/*
    Display sensor calibration status
*/
/**************************************************************************/
void displayCalStatus(void)
{
  /* Get the four calibration values (0..3) */
  /* Any sensor data reporting 0 should be ignored, */
  /* 3 means 'fully calibrated" */
  uint8_t system, gyro, accel, mag;
  system = gyro = accel = mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);

  /* The data should be ignored until the system calibration is > 0 */
  if (!system)
  {
    Serial.print("! ");
  }

  /* Display the individual values */
  Serial.print("Sys:");
  Serial.print(system, DEC);
  Serial.print(" G:");
  Serial.print(gyro, DEC);
  Serial.print(" A:");
  Serial.print(accel, DEC);
  Serial.print(" M:");
  Serial.println(mag, DEC);
}

bool saveCalib()
{
  // get current sensor offsets
  adafruit_bno055_offsets_t offsets;
  if (!bno.getSensorOffsets(offsets))
  {
    Serial.println("Cannot retrieve sensor offsets. Abort loading calib!");
    return false;
  }

  // get current calib state
  uint8_t system, gyro, accel, mag;
  system = gyro = accel = mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);

  // open file
  File file = SPIFFS.open(calibFile, FILE_WRITE);

  if (!file)
  {
    Serial.printf("Cannot open %s to save calib!\n", calibFile);
    return false;
  }

  // create JSON
  StaticJsonDocument<512> doc;

  doc["system"]["calib"] = system;

  doc["accel"]["x"] = offsets.accel_offset_x;
  doc["accel"]["y"] = offsets.accel_offset_y;
  doc["accel"]["z"] = offsets.accel_offset_z;
  doc["accel"]["r"] = offsets.accel_radius;
  doc["accel"]["calib"] = accel;

  doc["gyro"]["x"] = offsets.gyro_offset_x;
  doc["gyro"]["y"] = offsets.gyro_offset_y;
  doc["gyro"]["z"] = offsets.gyro_offset_z;
  doc["gyro"]["calib"] = gyro;

  doc["mag"]["x"] = offsets.mag_offset_x;
  doc["mag"]["y"] = offsets.mag_offset_y;
  doc["mag"]["z"] = offsets.mag_offset_z;
  doc["mag"]["r"] = offsets.mag_radius;
  doc["mag"]["calib"] = mag;

  // Serialize JSON to file
  serializeJson(doc, Serial);
  if (serializeJson(doc, file) == 0)
  {
    Serial.println(F("Failed to write to file"));
    file.close();
    return false;
  }
  file.close();

  return true;
}

bool loadCalib()
{
  File file = SPIFFS.open(calibFile, FILE_READ);

  StaticJsonDocument<512> doc;

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, file);
  if (error)
  {
    Serial.println(F("Failed to read file, using default calibration"));
    return false;
  }

  // print loaded JSON
  serializeJson(doc, Serial);

  adafruit_bno055_offsets_t offsets;

  // Copy values from the JsonDocument to the Config
  offsets.accel_offset_x = doc["accel"]["x"];
  offsets.accel_offset_y = doc["accel"]["y"];
  offsets.accel_offset_z = doc["accel"]["z"];
  offsets.accel_radius = doc["accel"]["r"];

  offsets.gyro_offset_x = doc["gyro"]["x"];
  offsets.gyro_offset_y = doc["gyro"]["y"];
  offsets.gyro_offset_z = doc["gyro"]["z"];

  offsets.mag_offset_x = doc["mag"]["x"];
  offsets.mag_offset_y = doc["mag"]["y"];
  offsets.mag_offset_z = doc["mag"]["z"];
  offsets.mag_radius = doc["mag"]["r"];

  Serial.println();
  Serial.printf("Accel: %d %d %d %d\n", offsets.accel_offset_x, offsets.accel_offset_y, offsets.accel_offset_z, offsets.accel_radius);
  Serial.printf("Gyro: %d %d %d\n", offsets.gyro_offset_x, offsets.gyro_offset_y, offsets.gyro_offset_z);
  Serial.printf("Mag: %d %d %d %d\n", offsets.mag_offset_x, offsets.mag_offset_y, offsets.mag_offset_z, offsets.mag_radius);

  // set new offsets
  bno.setSensorOffsets(offsets);

  return true;
}

void printFileSystem()
{
  Serial.printf("Using %d/%d bytes\n", SPIFFS.usedBytes(), SPIFFS.totalBytes());

  File root = SPIFFS.open("/");
  File file = root.openNextFile();

  while (file)
  {
    Serial.println(file.name());
    file = file.openNextFile();
  }
}

bool saveConfig()
{
  // open file
  File file = SPIFFS.open(configFile, FILE_WRITE);

  if (!file)
  {
    Serial.printf("Cannot open %s to save config!\n", configFile);
    return false;
  }

  // create JSON
  StaticJsonDocument<512> doc;

  doc["config"]["name"] = sensorName;
  doc["osc"]["host"] = osc_host;
  doc["osc"]["port"] = osc_port;

  // Serialize JSON to file
  serializeJson(doc, Serial);
  if (serializeJson(doc, file) == 0)
  {
    Serial.println(F("Failed to write to file"));
    file.close();
    return false;
  }

  file.close();
  return true;
}

bool loadConfig()
{
  File file = SPIFFS.open(configFile, FILE_READ);

  StaticJsonDocument<512> doc;

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, file);
  if (error)
  {
    Serial.println(F("Failed to read config file, using default configuration"));
    return false;
  }

  // print loaded JSON
  serializeJson(doc, Serial);

  // Copy values from the JsonDocument to the Config
  sensorName = String(doc["config"]["name"].as<const char *>());
  osc_host = String(doc["osc"]["host"].as<const char *>());
  osc_port = doc["osc"]["port"];

  return true;
}