#include <Arduino.h>
#include <ArduinoJson.h>
#include <Filters.h>
#include <RingBuf.h>
#include <FS.h> //this needs to be first, or it all crashes and burns...
#if defined(ESP8266)
#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino
#else
#include <WiFi.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <ArduinoOSCWiFi.h>

#define SENSOR_RAW 33
#define SENSOR_SIG 35
#define LED_BUILTIN 22

#define DELAY 50
// #define FREQ_AVG_COUNT 4
// #define MAP_BUFSIZE 4
#define LOWPASS_FREQ (1000. / DELAY)
#define WINDOW 20.0 / LOWPASS_FREQ


FilterOnePole muscleLowPass(LOWPASS, LOWPASS_FREQ);
RunningStatistics filterMuscleOneLowpassStats; // create running statistics to smooth these values

// values
uint16_t muscle_raw = 0;
float muscle_mean = 0.;
uint16_t muscle_sig = 0;

uint8_t rssi = 0;

// JSON
StaticJsonDocument<300> jsonBuffer;

// WiFI stuff
AsyncWebServer server(80);
DNSServer dns;
AsyncWiFiManager wifiManager(&server, &dns);

// OSC
int osc_port = 5460;
String osc_host = "192.168.100.255";

void setup()
{
  // setup serial
  Serial.begin(115200);

  // setup GPIO
  // analogReadResolution(10);
  ledcSetup(0, 5000, 12);
  ledcAttachPin(LED_BUILTIN, 0);

// pinMode(LED_BUILTIN, OUTPUT);
  //   digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  // delay(1000);                       // wait for a second
  // digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  // delay(1000);                       // wait for a second

  filterMuscleOneLowpassStats.setWindowSecs(WINDOW);

  // setup WiFi
  String wifiName = "muscle-sensor-" + String(SENSOR_NAME);

  if (!wifiManager.autoConnect(wifiName.c_str()))
  {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
  }

  Serial.println(WiFi.localIP());
  Serial.println(WiFi.broadcastIP());
  // set OSC host to broadcast
  osc_host = WiFi.broadcastIP().toString();

  // OscWiFi.publish(osc_host, osc_publish_port, "/breath/wind/min", wind_min)->setFrameRate(20.f);
  // OscWiFi.publish(osc_host, osc_publish_port, "/breath/wind/max", wind_max)->setFrameRate(20.f);
  OscWiFi.publish(osc_host, osc_port, String("/muscle/") + SENSOR_NAME + "/raw", muscle_raw)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, String("/muscle/") + SENSOR_NAME + "/mean", muscle_mean)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, String("/muscle/") + SENSOR_NAME + "/sig", muscle_sig)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, String("/muscle/") + SENSOR_NAME + "/rssi", rssi)->setFrameRate(5.f);
}

void loop()
{
  // get data
  muscle_raw = analogRead(SENSOR_RAW);
  muscle_sig = analogRead(SENSOR_SIG);

  // update rssi
  rssi = WiFi.RSSI();

  // filter
  muscleLowPass.input(muscle_raw);
  filterMuscleOneLowpassStats.input(muscleLowPass.output());
  muscle_mean = filterMuscleOneLowpassStats.mean();

  // print data
  // Serial.print(1023-raw);
  jsonBuffer["raw"] = muscle_raw;
  jsonBuffer["raw"] = muscle_mean;
  jsonBuffer["sig"] = muscle_sig;
  jsonBuffer["rssi"] = rssi;

  serializeJson(jsonBuffer, Serial);
  Serial.println();

  // set LED state
  ledcWrite(0, muscle_raw);

  // update OSC
  OscWiFi.update();

  delay(DELAY);
}