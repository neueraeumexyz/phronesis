import os
import sys
import argparse
from threading import Thread, Lock, Event
from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import udp_client
import bluepy
from bluepy import btle
import binascii
import struct

class BLEHeartRate(bluepy.btle.DefaultDelegate):

    def __init__(self, name, osc_client):
        super().__init__()
        self.name = name
        self.osc_client = osc_client

    def handleNotification(self, cHandle, data):
        print("Notification:", cHandle, "sent data", binascii.b2a_hex(data), binascii.b2a_uu(data))
        print(type(data))
        print(data)
        if len(data) == 2:
            t, bpm = struct.unpack("BB", data)
            print("bpm: {:d}".format(bpm))

            if self.osc_client is not None:
                osc_client.send_message("/heartrate/{}/bpm".format(self.name), bpm)

    def handleDiscovery(self, scanEntry, isNewDev, isNewData):
        print("Discovered device", scanEntry.addr, scanEntry.getScanData())

if __name__ == "__main__":
    print("BLE Heartrate to OSC")
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",
                        default="0.0.0.0", help="Destination host for OSC messages")
    parser.add_argument("--port",
                        type=int, default=5005, help="Destination port for OSC messages")
    parser.add_argument('--devices', type=str, nargs='+', help='BLE MAC adresses', default=("c5:b5:82:b9:36:5c",))
    args = parser.parse_args()

    # setup OSC client
    osc_client = udp_client.SimpleUDPClient(args.host, args.port)


    # connect to BLE devices
    addresses = ("c5:b5:82:b9:36:5c", "fa:d7:bb:5e:33:d6")
    heartrate_uuid = btle.UUID(0x2a38)
    cccid = btle.AssignedNumbers.client_characteristic_configuration
    hrmid = btle.AssignedNumbers.heart_rate
    hrmmid = btle.AssignedNumbers.heart_rate_measurement

    periphals = []
    for device in args.devices:
        p = None
        try:
            print("Connect to {}".format(device))
            p = btle.Peripheral(device, addrType=btle.ADDR_TYPE_RANDOM)
            p.setDelegate(BLEHeartRate(device, osc_client))
            service, = [s for s in p.getServices() if s.uuid==hrmid]
            ccc, = service.getCharacteristics(forUUID=str(hrmmid))

            desc = p.getDescriptors(service.hndStart, service.hndEnd)
            d, = [d for d in desc if d.uuid==cccid]
            p.writeCharacteristic(d.handle, b'\1\0')
            print("Set up {}".format(device))
            periphals.append(p)
            # print("Setting Characteristics")
            # ch = p.getCharacteristics(uuid=heartrate_uuid)[0]
            # print("Setting Done, writing now")
            # ch.write(struct.pack('<bb', 0x01, 0x00))
            # print("writing Done, looping now")
            # while True:
            #     if p.waitForNotifications(1.0):
            #         print("Notification trigger")
            #         continue
            # print("Waiting")
        except btle.BTLEDisconnectError as e:
            print("Failed to connecting to {}:\n".format(device, e))
            if p is not None:
                p.disconnect()            

    while len(periphals) > 0:
        try:
            for p in periphals:
                if p.waitForNotifications(1.0):
                    # print("Notification trigger")
                    continue
        except KeyboardInterrupt:
            break

    for p in periphals:
        p.disconnect()
    # scanner = bluepy.btle.Scanner()
    # delegate = BLEHeartRate()
    # scanner.withDelegate(delegate)
    # devices = scanner.scan()

    # print()
    # for dev in devices:
    #     data = dev.getScanData()
    #     if dev.addr in addresses:
    #         print("device: {} {}".format(dev.addr, dev.getScanData()))
    #         connect(dev, dev.addr)
    #     # for idx, key, val in data:
    #     #     if idx == bluepy.btle.ScanEntry.COMPLETE_LOCAL_NAME in ("InfiniTime", "RDL52832") \
    #     #     or dev.addr in addresses:
    #     #         print("device: {} {}".format(dev.addr, dev.getScanData()))
    #     #         connect(dev, val)