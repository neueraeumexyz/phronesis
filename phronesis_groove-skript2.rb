#phronesis 2021 - groovegarden 2

use_bpm 120
#buffer(:foo, 48)
#b = buffer(:foo)
#puts b.duration

run_file "/home/pi/Desktop/sonicpi-mystuff/phronesis_mappingosc.rb"

live_loop :exhale1 do
  stop
  exhale = sync "/osc:127.0.0.1:*/breath/exhale"
  puts exhale[0]
  e = exhale[0]
  puts "exhale is", e
  relative = sync "/osc:127.0.0.1:*/breath/map"
  c = relative[0]
  puts "map is", c
  
  with_fx :lpf, cutoff: 80, amp: 0.7 do
    use_synth :tri
    play 36, amp: e*(0+get[:vari])/90.0, pan: rrand(-c, c),
      env_curve: 3, cutoff: (0+get[:mean]), attack: 0.5,
      release: 1, sustain: (0+get[:vari]/127.0)
  end
  
  with_fx :lpf, cutoff: 26 do
    with_fx :hpf, cutoff: 55 do
      use_synth :sine
      play 42, amp: e*(0+get[:vari])/60.0, pan: rrand(-c, c),
        env_curve: 3, cutoff: (0+get[:vari]), attack: 0.5,
        sustain: (0+get[:vari]/127.0), release: 1
    end
  end
  sleep 0.4
end

live_loop :vogelbeat1 do
  stop
  sleep 0.25
  if (spread 1,4).tick then
    sample "/home/pi/Desktop/Aufnahmen_phronesis/vogeldrum3.flac",
      amp: 8, sustain: 0.2, release: 0.5, #0.5,
      beat_stretch: 1, #5, #10
      rate: 0.25,
      start: 0.4 #[0.4, 0.5, 0.6].choose,
      attack: 0.75 #[0.75, 0.5, 0.25, 0.3].choose,
      end
end

#### muskel #####

with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.6 do
  live_loop :mel_arm do
    use_real_time
    stop
    use_synth :pretty_bell
    with_fx :hpf, cutoff: (0+get[:arm]/50), amp: 0.5 do
      play (0+get[:arm]), amp: 1 #0.5
      sleep 1
    end
  end
  
  live_loop :mel_leg do
    use_real_time #klavier
    stop
    use_synth :pluck
    with_fx :lpf, cutoff: (0+get[:leg]) do
      play (50+get[:leg]), sustain: 0.5, release: 0.5, amp: 2, pan: 1
      sleep 1
    end
  end
end

#### STOPP exhale1 ####
### change vogel beatstrech 10



live_loop :atem1 do
  use_real_time
  #stop
  relative = sync "/osc:127.0.0.1:*/breath/map"
  c = relative[0]
  puts "map is", c
  exhale = sync "/osc:127.0.0.1:*/breath/exhale"
  puts exhale[0]
  e = exhale[0]
  puts "exhale is", e
  with_fx :hpf, cutoff: 130 do
    with_fx :slicer, phase: 0.25, wave: 2, prob_pos: 1 do
      use_synth :gnoise
      play attack: 1, release: (0+get[:mean]/50.0), amp: e*4
      sleep 0.75
    end
  end
  use_synth :dark_ambience
  play (20+get[:vari]), amp: e*2, attack: 2, release: (0+get[:vari]/20.0), pan: rrand(-c, c)
  sleep 1
end

### add vogel .choose

live_loop :travelling do
  stop
  note  = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/note_on"
  velocity, duration = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/control_change"
  z = note[0]
  y = note[1]
  use_synth :beep
  #### STOPP Vogel +++ change num_octaves auf 3
  notes = scale(z, [:acem_asiran, :suznak_2, :ahirbhairav].choose, num_octaves: 0, invert: 1) #change num_octaves from 0-2
  use_random_seed 679
  sleep duration/5.0
  if (spread 1,4).tick then
    tick_reset_all
    with_fx :echo, phase: 0.125, mix: 0.4, reps: 16, amp: 0.4 do
      sleep duration/300.0
      play notes.choose, attack: 0, release: 0.1, pan: (range -1, 1, step: 0.125).tick,  amp_slide: 0.05,  amp: 4 # rrand(0.5, velocity/1000.0)
    end
    sleep duration/40.0
  end
end




