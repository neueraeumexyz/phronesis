#phronesis 2021 - groovegarden 1

use_bpm 80
#buffer(:foo, 128)
#b = buffer(:foo)
#puts b.duration

#with_fx :sound_out, amp: 16 do

run_file "/home/pi/Desktop/sonicpi-mystuff/phronesis_mappingosc.rb"


########################################################### part I #######

live_loop :pflanze2 do
  use_real_time
  stop
  note  = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/note_on"
  velocity, duration = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/control_change"
  z = note[0]
  y = note[1]
  use_synth :hollow
  with_fx :bpf, centre: 80, res: 0.4, amp: 0.7 do
    with_fx :wobble, phase: 0.8, cutoff_min: 60, cutoff_max: 120, res: 0.2, wave: 3, probability: 0.7, pre_amp: 0.5 do
      play z, attack: 0.1, attack_level: 0.2, decay: 0.3, decay_level: 0.3,
        sustain: 0.5, sustain_level: 0.7, release: 1.5, pan: rrand(-1, 1), amp: 2 #20
      sleep 1
    end
  end
end

#### muskel #####

with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.6 do
  live_loop :mel_arm do
    use_real_time
    stop
    use_synth :pretty_bell
    with_fx :hpf, cutoff: (0+get[:arm]/50), amp: 0.5 do
      play (0+get[:arm]), amp: 1 #10
      sleep 1
    end
  end
  
  live_loop :mel_leg do
    use_real_time #klavier
    stop
    use_synth :pluck
    with_fx :lpf, cutoff: (0+get[:leg]) do
      play (50+get[:leg]), sustain: 0.5, release: 0.5, amp: 2, pan: 1
      sleep 1
    end
  end
end


# STOP::: osc-relay +++ mel_leg
# mel_arm amp: 10


############################################# part II #####################

live_loop :tropfen , sync: :mel_arm do
  stop
  note  = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/note_on"
  velocity, duration = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/control_change"
  z = note[0]
  y = note[1]
  if (spread 1,4).tick then
    with_fx :lpf, cutoff: 100 do
      use_synth :sine
      with_fx :reverb, room: 0.9, amp: 0.5, mix: 0.6 do
        play note, amp: 2, pan: rrand(-1, 1), env_curve: 2
        sleep 1 #duration/250.0
      end
    end
  end
end


live_loop :pflanze1, sync: :tropfen  do
  stop
  note  = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/note_on"
  velocity, duration = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/control_change"
  z = note[0]
  y = note[1]
  if (spread 3,8).tick then
    with_fx :rhpf, cutoff: 130, res: 0.4 do #cutooff:130, variable, amp angleichen ! /40 -- /127
      with_synth :pnoise do
        play z, attack: 0.05, decay: 0.08, release: duration/10.0, amp: velocity/20.0
      end
      #end
    end
  end
  sleep 0.25
end

# fade out "planze2"

live_loop :lichtstrahl do
  stop
  note  = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/note_on"
  velocity, duration = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/control_change"
  z = note[0]
  y = note[1]
  sleep duration/5.0
  use_synth :dark_ambience
  with_fx :flanger, mix: 0.7 do
    play choose([z, y]), attack: 5, release: 5, amp: velocity/60.0, room: 60, pan: rrand(-1, 1)
    sleep 2
  end
end

####### part III ########

# stop "lichtstrahl", "pflanze1"

live_loop :vogelbeat1, sync: :mel_arm do
  stop
  sleep 1
  if (spread 1,4).tick then
    sample "/home/pi/Desktop/Aufnahmen_phronesis/vogeldrum3.flac",
      amp: 16, rate: [0.75, 0.5, 0.25, 1].choose,
      start: [0.4, 0.5, 0.6].choose, attack: [0.75, 0.5, 0.25, 0.3].choose,
      sustain: 0.2, release: 0.3, beat_stretch: 10,
      rate: 0.5
    
    sample :ambi_glass_hum,
      rate: [-0.75, -0.5, -1].choose,
      attack: 1.5, sustain: 0.3, release: 3.5, amp: 6, #10
      pan: rrand(-1,1)
  end
  sleep 0.5
end


live_loop :travelling do
  stop
  note  = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/note_on"
  velocity, duration = sync "/midi:usb_midi_interface_usb_midi_interface_midi_1_28_0:4/control_change"
  z = note[0]
  y = note[1]
  use_synth :beep
  notes = scale(z, [:acem_asiran, :suznak_2, :ahirbhairav].choose, num_octaves: 0, invert: 1) #change num_octaves from 0-2
  #use_random_seed 679
  sleep duration/5.0
  if (spread 1,4).tick then
    tick_reset_all
    with_fx :echo, phase: 0.125, mix: 0.4, reps: 16, amp: 0.4 do
      sleep duration/300.0
      play notes.choose, attack: 0, release: 0.1, pan: (range -1, 1, step: 0.125).tick,  amp_slide: 0.05,  amp: rrand(0.5, velocity/1000.0)
    end
    sleep duration/70.0
  end
end


################# atem ###################### ALLES AUS




########### part IV ########## STOP ALL




#end






