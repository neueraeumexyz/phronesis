import argparse
import glob
import json
import logging
import time
from threading import Thread, Event

import serial
from pythonosc.osc_message import OscMessage
from pythonosc.osc_message_builder import OscMessageBuilder
from pythonosc.udp_client import UDPClient
from serial.serialutil import SerialException

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("USB2OSC")

class USB2OSCServer:

    def __init__(self, oscHost: str, oscPort: int, topic: str = "movement"):
        self._client: UDPClient = UDPClient(oscHost, oscPort, True)
        self._topic: str = topic
        self._usbDevices: dict[str, serial.Serial] = {}
        self._errorsMax: int = 5

        self._workers: dict[str, Thread] = {}
        self._stopped: Event = Event()

    def Serve(self) -> None:
        self.Stop()
        self._usbDevices = self.GetUSBDevices(list(self._usbDevices.keys()))
        self._stopped.clear()
        for deviceName, device in self._usbDevices.items():
            self._workers[deviceName] = Thread(target=self._Run, args=(deviceName, device), daemon=True)
            self._workers[deviceName].start()
        self._stopped.wait()

    def Stop(self) -> None:
        self._stopped.set()
        for worker in self._workers.values():
            worker.join(2.)
        self._workers.clear()

    def _Run(self, name:str, device: serial.Serial):
        errorCount = 0
        while not self._stopped.is_set():
            try:
                msg = device.readline().decode().strip()
                logger.info(f"{name}: Got message {msg}")
                for oscMessage in self._DecodeMessage(msg):
                    self._client.send(oscMessage)
            except SerialException as e:
                errorCount += 1
                logger.error(f"Error {errorCount}/{self._errorsMax} on device {name}: {e}")

                if errorCount >= self._errorsMax:
                    logger.error("Too many errors. Stopping...")
                    self._stopped.set()
                else:
                    time.sleep(1.)

    def _DecodeMessage(self, msg: str) -> list[OscMessage]:
        oscMessages: list[OscMessage] = []
        try:
            data = json.loads(msg)
            sensorName = data["sensor"]["name"]
            for sensor in data["imu"]:
                for axis, value in data["imu"][sensor].items():
                    osc = OscMessageBuilder(f"/{self._topic}/{sensorName}/{sensor}/{axis}")
                    osc.add_arg(value)
                    oscMessages.append(osc.build())
        except Exception:
            logger.error("Cannot decode {msg}: {e}")

        return oscMessages

    @staticmethod
    def GetUSBDevices(ignore: list[str]) -> dict[str, serial.Serial]:
        """
        Discovers all ttyUSB and ttyACM serial devices and opens them.

        :return: dictionary with device_path: serial_port
        """
        devices: dict[str, serial.Serial] = {}
        for path in ("/dev/ttyUSB*", "/dev/ttyACM*"):
            for dev in glob.glob(path):
                if dev not in ignore:
                    devices[dev] = serial.Serial(dev, baudrate=115200)

        return devices


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",
                        default="127.0.0.1", help="The ip to send the messages to.")
    parser.add_argument("--port",
                        type=int, default=5460, help="The port to send the messages to.")
    args = parser.parse_args()

    server = USB2OSCServer(args.host, args.port)

    # run in endless loop to restart on errors
    while True:
        try:
            server.Serve()
        except KeyboardInterrupt:
            server.Stop()

