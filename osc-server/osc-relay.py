from pythonosc import dispatcher
from pythonosc import osc_server
from pythonosc import udp_client

if __name__ == "__main__":
    client = udp_client.SimpleUDPClient('127.0.0.1', 4560)


    def default_handler(address, *args):
        print(f"DEFAULT {address}: {args}")
        client.send_message(address, args)


    dispatcher = dispatcher.Dispatcher()
    dispatcher.set_default_handler(default_handler)

    server = osc_server.ThreadingOSCUDPServer(('0.0.0.0', 5460), dispatcher)
    print("Serving on {}".format(server.server_address))
    server.serve_forever()
