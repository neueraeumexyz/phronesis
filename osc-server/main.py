import sys
import os
import argparse
from datetime import datetime
from threading import Lock, Event
from collections import deque, OrderedDict
from typing import Any, Optional, List, Dict
from pythonosc import dispatcher
from pythonosc import osc_server
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import csv
import logging
import time
import json


values = dict()
event = Event()

logging.basicConfig(level=logging.INFO)


class Recorder:

    def __init__(self, filename: str):
        self._filenamne = filename
        self._data: List[Dict] = []
        self._lock = Lock()

    def write(self, timestamp, topic, value) -> None:
        with self._lock:
            try:
                self._data.append({'timestamp': timestamp,
                                   'topic': topic,
                                   'value': value})
            except Exception as e:
                print(e)

    def close(self) -> None:
        if not self._data:
            return
        with self._lock:
            with open(self._filenamne, 'wt') as f:
                self._writeToFile(f)

    def _writeToFile(self, fp):
        json.dump(self._data, fp)

    def getFilename(self) -> str:
        return self._filenamne


class CSVRecorder(Recorder):

    def _writeToFile(self, fp):
        fieldnames = self._data[0].keys()
        writer = csv.DictWriter(fp, fieldnames=fieldnames)
        writer.writeheader()

        for row in self._data:
            writer.writerow(row)


class Worker(QtCore.QObject):

    finished = QtCore.pyqtSignal()
    # progress = QtCore.pyqtSignal(int)
    update = QtCore.pyqtSignal(str, float)

    def __init__(self, host, port, *args, **kwargs):
        super(Worker, self).__init__(*args, **kwargs)
        self._host = host
        self._port = port
        self._server = None

    def send_update(self, topic: str, value: Any):
        # print("send update")
        self.update.emit(topic, value)

    def run(self):
        disp = dispatcher.Dispatcher()
        disp.set_default_handler(self.send_update)
        # disp.map("/breath/raw", self.send_update)
        # disp.map("/breath/map", self.send_update)
        # disp.map("/breath/min", self.send_update)
        # disp.map("/breath/max", self.send_update)
        # disp.map("/breath/mean", self.send_update)
        # disp.map("/breath/exhale", self.send_update)
        # disp.map("/breath/derivative", self.send_update)
        # disp.map("/breath/temp", self.send_update)
        # disp.map("/breath/rssi", self.send_update)

        self._server = osc_server.ThreadingOSCUDPServer(
            (self._host, self._port), disp)
        logging.info(f'Serving on {self._server.server_address}')
        self._server.serve_forever()
        self.finished.emit()


class App(pg.GraphicsLayoutWidget):
    def __init__(self, host, port, queue_length, record_path='.', parent=None):
        super(App, self).__init__(parent)

        self.host = host
        self.port = port
        self.queue_length = queue_length
        self.record_path = record_path
        self.t_start = time.time()

        # self.mainbox = QtGui.QWidget()
        # self.setCentralWidget(self.mainbox)
        #
        # self.canvas = pg.GraphicsLayoutWidget()
        # self.label = QtGui.QLabel()
        #
        # lay = QtGui.QVBoxLayout(self.mainbox)
        # lay.addWidget(self.canvas)
        # lay.addWidget(self.label)
        # self.win = pg.GraphicsWindow(title="OSC plotting")
        # self.win.resize(1000, 600)

        self.items = OrderedDict()
        self.values = dict()

        # for i in range(4):
        #     view = self.canvas.addViewBox()
        #     view.setAspectLocked(True)
        #     view.setRange(QtCore.QRectF(0, 0, 10, 10))
        #     it = pg.ImageItem(None, border="w")
        #     view.addItem(it)
        #     self.img_items.append(it)
        #     self.canvas.nextRow()

        timer = QtCore.QTimer(self, interval=1)
        timer.timeout.connect(self._update)
        timer.start()

        # self.thread = Thread(target=self.start_server, args=(self.host, self.port))
        # self.thread.run()
        # Step 2: Create a QThread object
        self.thread = QtCore.QThread()
        # Step 3: Create a worker object
        self.worker = Worker(self.host, self.port)
        # Step 4: Move worker to the thread
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.worker.update.connect(self.append)
        self.thread.start()

        # recorder
        self._recorder: Optional[Recorder] = None

    def keyPressEvent(self, ev):
        self.scene().keyPressEvent(ev)
        if ev.key() == QtCore.Qt.Key.Key_Space:
            logging.info("reset data")
            for key in self.items.keys():
                self.values[key].clear()
            self.t_start = time.time()
        elif ev.key() == QtCore.Qt.Key.Key_Return:
            # start recording
            if self._recorder is not None:
                logging.info(f"Recording to {self._recorder.getFilename()} stopped")
                self._recorder.close()
                self._recorder = None
                self.setBackground((0, 0, 0))
            else:
                filename = f"{datetime.now().strftime('%Y%m%d_%H%M%S')}.csv"
                # self._recorder = Recorder(filename)
                self._recorder = CSVRecorder(os.path.join(self.record_path, filename))
                self.setBackground((50, 0, 0))
                logging.info(f"Recording to {filename}")
        elif ev.key() == QtCore.Qt.Key.Key_Escape:
            self.close()

    def _update(self):
        for key, item in self.items.items():
            # data = np.random.rand(10, 10)
            # item.setImage(self.values[key])
            item.listDataItems()[0].setData([v[0] for v in self.values[key]], [v[1] for v in self.values[key]])

    def append(self, topic, value):
        # create new deque for new topic and save value to deque
        if topic not in self.values.keys():
            self.values[topic] = deque(maxlen=self.queue_length)
        self.values[topic].append((time.time()-self.t_start, value))

        # add new topic plot
        if topic not in self.items.keys():
            self.items[topic] = self.addPlot(title=topic)
            self.items[topic].plot()
            # self.items[topic] = self.addPlot(title=topic).plot()
            self.nextRow()
        self.items[topic].setTitle(f"{topic} {value:.2f}")

        # add to recording
        if self._recorder is not None:
            self._recorder.write(time.time(), topic, value)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host",
                        default="0.0.0.0", help="The ip to listen on")
    parser.add_argument("--port",
                        type=int, default=5005, help="The port to listen on")
    parser.add_argument("--queue",
                        type=int, default=200, help="Queue size")
    parser.add_argument("--record",
                        type=str, default=".", help="Recording path")
    args = parser.parse_args()

    app = QtGui.QApplication(sys.argv)
    thisapp = App(args.host, args.port, args.queue, args.record)
    thisapp.show()
    sys.exit(app.exec_())
