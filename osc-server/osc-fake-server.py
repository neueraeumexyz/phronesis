import random
import time
from dataclasses import dataclass
from threading import Event, Thread
from typing import Tuple, List
import signal

from pythonosc.osc_message_builder import OscMessageBuilder
from pythonosc.udp_client import SimpleUDPClient


@dataclass
class FakeTopic:
    topic: str
    frequency: float
    dataRange: Tuple[float, float] = (0., 1.)


class OSCDataTopic(Thread):

    def __init__(self, serverAddr: Tuple[str, int], topic: str, frequency: float, dataRange: Tuple[float, float] = (0., 1.)):
        super().__init__(name=f'Thread_{topic}')
        self._serverAddr = serverAddr
        self._topic = topic
        self._frequency = frequency
        self._dataRange = dataRange
        self._stopped: Event = Event()
        self._client = SimpleUDPClient(*self._serverAddr)

    def run(self) -> None:
        self._stopped.clear()
        waitTime = 1./self._frequency
        while not self._stopped.is_set():
            msg = OscMessageBuilder(self._topic)
            msg.add_arg(random.uniform(*self._dataRange))
            self._client.send(msg.build())
            time.sleep(waitTime)

    def shutdown(self):
        self._stopped.set()


def default_handler(address, *args):
    print(f"DEFAULT {address}: {args}")


if __name__ == "__main__":
    address = ('127.0.0.1', 5005)

    # create and start clients
    clients = list()
    clients.append(OSCDataTopic(address, '/fakedata/x', 10.))
    clients.append(OSCDataTopic(address, '/fakedata/y', 10.))
    clients.append(OSCDataTopic(address, '/fakedata/z', 10.))

    for client in clients:
        client.start()

    try:
        signal.pause()
    except KeyError:
        for client in clients:
            client.shutdown()
