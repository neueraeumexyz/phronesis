#include <Arduino.h>
#include <FS.h> //this needs to be first, or it all crashes and burns...
#include <pcf8563.h>
#include <TFT_eSPI.h> // Graphics and font library for ST7735 driver chip
#include <SPI.h>
#include <Wire.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <ArduinoOSCWiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <RemoteDebug.h>
#include <ArduinoJson.h>
#include <RingBuf.h>
#include "sensor.h"
#include "esp_adc_cal.h"
#include "ttgo.h"
#include "charge.h"
#include <MAX30105.h>
#include <heartRate.h>

#define TP_PIN_PIN 33
#define I2C_SDA_PIN 21
#define I2C_SCL_PIN 22
#define IMU_INT_PIN 38
#define RTC_INT_PIN 34
#define BATT_ADC_PIN 35
#define VBUS_PIN 36
#define TP_PWR_PIN 25
#define CHARGE_PIN 32
#define TFT_BL 27

#define LED_PIN 4
#define LED_CHANNEL 0
#define LED_MIN 0
#define LED_MAX 512
#define LED_STEP 10

#define HEATRATE_SDA 15
#define HEATRATE_SCL 13
#define HEATRATE_INT 4

#define OSC_HEARTRATE_RAW_FPS 30.0
#define OSC_HEARTRATE_BPM_FPS 1.0
#define OSC_IMU_FPS 10.0
#define OSC_BAT_FPS 1.

#define DELAY 0

extern MPU9250 IMU;

extern "C"
{
  uint8_t temprature_sens_read();
}

// debug console
RemoteDebug Debug;

// devices
TFT_eSPI tft = TFT_eSPI(); // Invoke library, pins defined in User_Setup.h
PCF8563_Class rtc;
MAX30105 heartSensor;

// values
uint8_t heartrate_bpm = 0;
uint32_t heartrate_raw = 0;
uint8_t heartrate_beat = 0;
float heartrate_map = 0;
float bat_voltage = 0;
uint8_t temperature_internal = 0;

#define BUFSIZE 100
RingBuf<uint32_t, BUFSIZE> heartBuf;

const byte RATE_SIZE = 4; //Increase this for more averaging. 4 is good.
byte rates[RATE_SIZE];    //Array of heart rates
byte rateSpot = 0;
long lastBeat = 0; //Time at which the last beat occurred
float beatsPerMinute;
int beatAvg;

int8_t rssi = 0;
String sensor_name;
uint8_t hh, mm, ss;
int vref = 1100;

// JSON
StaticJsonDocument<300> jsonBuffer;

// WiFI stuff
AsyncWebServer server(80);
DNSServer dns;
AsyncWiFiManager wifiManager(&server, &dns);

// OSC
int osc_port = 5460;
// char *osc_port_str = "54445";
String osc_host = "192.168.100.255";

// states
enum WATCH_STATES
{
  NONE,
  WIFI_CONNECTING,
  WIFI_PORTAL,
  RUNNING,
  UPDATING,
  SLEEP
};
WATCH_STATES state_current = NONE;

enum LED_STATES
{
  LED_NONE,
  LED_OFF,
  LED_ON,
  LED_FLASHING,
  LED_FADING,
  LED_HEARTRATE
};
LED_STATES state_led = LED_OFF;

// declarations
void taskButton(void *parameter);
void taskLED(void *parameter);
void setupOTA();
void setupADC();
void setupRTC();
float getVoltage();
void startDeepsleep();
float min(RingBuf<uint32_t, BUFSIZE> &buf);
float max(RingBuf<uint32_t, BUFSIZE> &buf);

/***************************************************************
 * @brief Setup routine
 * 
 **************************************************************/
void setup()
{
  // setup serial
  Serial.begin(115200);

  // setup LED
  pinMode(LED_PIN, OUTPUT);
  ledcSetup(LED_CHANNEL, 5000, 10);
  ledcAttachPin(LED_PIN, LED_CHANNEL);

  // gpio_hold_en(GPIO_NUM_4);
  gpio_deep_sleep_hold_dis();

  // setup button
  pinMode(TP_PIN_PIN, INPUT);
  //! Must be set to pull-up output mode in order to wake up in deep sleep mode
  pinMode(TP_PWR_PIN, PULLUP);
  digitalWrite(TP_PWR_PIN, HIGH);

  xTaskCreate(
      taskButton,   /* Task function. */
      "TaskButton", /* String with name of task. */
      10000,        /* Stack size in bytes. */
      NULL,         /* Parameter passed as input of the task */
      1,            /* Priority of the task. */
      NULL);        /* Task handle. */

  xTaskCreate(
      taskLED,   /* Task function. */
      "TaskLED", /* String with name of task. */
      10000,     /* Stack size in bytes. */
      NULL,      /* Parameter passed as input of the task */
      1,         /* Priority of the task. */
      NULL);     /* Task handle. */


  // TFT Backlight
  // ledcSetup(1, 5000, 8);
  // ledcAttachPin(TFT_BL, 1);
  // ledcWrite(1, 185);
  // pinMode(TFT_BL, OUTPUT);
  // digitalWrite(TFT_BL, 0);

  // tft.init();
  // tft.setRotation(1);
  // tft.setSwapBytes(true);
  // tft.pushImage(0, 0,  160, 80, ttgo);

  // Wire.begin(I2C_SDA_PIN, I2C_SCL_PIN);
  // Wire.setClock(400000);

  // setup stuff
  setupRTC();
  // setupMPU9250();
  setupADC();

  // setup WiFi
  sensor_name = "heart-" + WiFi.macAddress();

  state_current = WIFI_CONNECTING;
  if (!wifiManager.autoConnect(sensor_name.c_str()))
  {
    Serial.println("failed to connect and hit timeout");
    state_current = WIFI_PORTAL;
    delay(3000);
  }
  state_current = RUNNING;

  Serial.println(WiFi.localIP());
  Serial.println(WiFi.broadcastIP());
  // set OSC host to broadcast
  osc_host = WiFi.broadcastIP().toString();

  // MDNS
  if (MDNS.begin(sensor_name.c_str()))
  {
    Serial.print("* MDNS responder started. Hostname -> ");
    Serial.println(sensor_name);
  }

  MDNS.addService("telnet", "tcp", 23);

  // Debug
  Debug.begin(sensor_name); // Initialize the WiFi server
  //Debug.setPassword("r3m0t0."); // Password on telnet connection ?
  Debug.setResetCmdEnabled(true); // Enable the reset command
  Debug.showProfiler(true);       // Profiler (Good to measure times, to optimize codes)
  Debug.showColors(true);         // Colors
  Debug.println("Debugger started!");

  // Initialize sensor
  Wire1.begin(HEATRATE_SDA, HEATRATE_SCL);
  if (!heartSensor.begin(Wire1, I2C_SPEED_FAST)) //Use default I2C port, 400kHz speed
  {
    Debug.println("MAX30102 was not found. Please check wiring/power. ");
  }
  Debug.println("Place your index finger on the sensor with steady pressure.");

  heartSensor.setup();                      //Configure sensor with default settings
  heartSensor.setPulseAmplitudeRed(0x0A);   //Turn Red LED to low to indicate sensor is running
  heartSensor.setPulseAmplitudeGreen(0x00); //Turn off Green LED
  // heartSensor.setPulseAmplitudeIR(0x7F);  //Turn off Green LED

  // setup OTA Update
  setupOTA();

  // OscWiFi.publish(osc_host, osc_port, "/breath/raw", wind_raw)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/beat", heartrate_beat)->setFrameRate(OSC_HEARTRATE_RAW_FPS);
  OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/raw", heartrate_raw)->setFrameRate(OSC_HEARTRATE_RAW_FPS);
  OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/bpm", heartrate_bpm)->setFrameRate(OSC_HEARTRATE_BPM_FPS);
  OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/bat", bat_voltage)->setFrameRate(OSC_BAT_FPS);
  OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/rssi", rssi)->setFrameRate(OSC_BAT_FPS);
  OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/temperature", temperature_internal)->setFrameRate(OSC_BAT_FPS);

  // IMU
  // OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/imu/gyro/x", IMU.gx)->setFrameRate(OSC_IMU_FPS);
  // OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/imu/gyro/", IMU.gy)->setFrameRate(OSC_IMU_FPS);
  // OscWiFi.publish(osc_host, osc_port, "/heart/" + WiFi.macAddress() + "/imu/gyro/z", IMU.gz)->setFrameRate(OSC_IMU_FPS);
}

/***************************************************************
 * @brief Loop
 * 
 **************************************************************/
void loop()
{
  ArduinoOTA.handle();

  // stop loop if we go to sleep mode
  if (state_current == SLEEP || state_current == UPDATING)
  {
    return;
  }

  if(!WiFi.isConnected()){
    WiFi.reconnect();
  }

  // get heartrate
  heartrate_raw = heartSensor.getIR();
  bat_voltage = getVoltage();

  if (checkForBeat(heartrate_raw))
  {
    //We sensed a beat!
    heartrate_beat = 1;
    long delta = millis() - lastBeat;
    lastBeat = millis();

    beatsPerMinute = 60 / (delta / 1000.0);

    if (beatsPerMinute < 255 && beatsPerMinute > 20)
    {
      rates[rateSpot++] = (byte)beatsPerMinute; //Store this reading in the array
      rateSpot %= RATE_SIZE;                    //Wrap variable

      //Take average of readings
      heartrate_bpm = 0;
      for (byte x = 0; x < RATE_SIZE; x++)
        heartrate_bpm += rates[x];
      heartrate_bpm /= RATE_SIZE;
    }
  }
  else
  {
    heartrate_beat = 0;
  }

  // filter
  // auto heartMin = min(heartBuf);
  // auto heartMax = max(heartBuf);
  // if (heartBuf.isFull())
  // {
  //   uint32_t tmp;
  //   heartBuf.pop(tmp);
  // }
  // heartBuf.push(heartrate_raw);
  // heartrate_map = map(heartrate_raw, heartMin, heartMax, 0, 1023) / 1024.;

  // // print data
  // // Serial.print(1023-raw);
  // jsonBuffer["bpm"] = beatAvg;
  // jsonBuffer["raw"] = heartrate_raw;
  // jsonBuffer["bat"] = bat_voltage;
  // // ledcWrite(LED_CHANNEL, heartrate_raw);

  // serializeJson(jsonBuffer, Serial);
  // Serial.println();

  // serializeJson(jsonBuffer, Debug);
  // Debug.println();

  // set LED state
  //analogWrite(LED, wind_map*255.0);
  // ledcWrite(LED, wind_map * 1024);

  // update temperature
  temperature_internal = (temprature_sens_read() - 32) / 1.8;

  // update rssi
  rssi = WiFi.RSSI();

  // update IMU
  readMPU9250();

  // update OSC
  OscWiFi.update();

  // update Debug
  Debug.handle();

  // delay(DELAY);
}

void taskButton(void *parameter)
{
  int button_last = LOW;
  auto button_pressed_last = millis();

  uint32_t button_pressed_short_limit = 10;
  uint32_t button_pressed_long_limit = 1000;

  while (true)
  {
    auto button = digitalRead(TP_PIN_PIN);

    // pressed change
    if (button == HIGH && button_last == LOW)
    {
      button_pressed_last = millis();
    }
    else if (button == LOW && button_last == HIGH)
    {
      // released
      auto deltaT = millis() - button_pressed_last;
      if ((deltaT > button_pressed_short_limit))
      {
        Debug.println("Next state [deepsleep]");
        Debug.handle();
        Serial.println("Go to deepsleep....");
        startDeepsleep();
      }
    }

    // check pressed long
    if (button == HIGH && (millis() - button_pressed_last > button_pressed_long_limit))
    {
      Debug.println("long press restart");
      Debug.handle();
      ESP.restart();
    }

    button_last = button;

    //   switch (state_current)
    //   {
    //   case WIFI_CONNECTING:
    //   case WIFI_PORTAL:

    //     break;

    //   default:
    //     break;
    //   }
    // {
    //   // esp_restart();
    //   Serial.println("Go to deepsleep....");
    //   esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, HIGH);
    //   delay(200);
    //   esp_deep_sleep_start();
    // }
    Debug.handle();
    delay(10);
  }

  vTaskDelete(NULL);
}
void taskLED(void *parameter)
{
  auto last = millis();
  int16_t led_step = LED_STEP;

  while (true)
  {
    // set LED state
    switch (state_current)
    {
    case WIFI_CONNECTING:
      state_led = LED_FADING;
      break;
    case WIFI_PORTAL:
      state_led = LED_ON;
      break;
    case RUNNING:
      // state_led = LED_HEARTRATE;
      state_led = LED_FLASHING;
      // state_led = LED_FADING;
      break;
    case UPDATING:
      state_led = LED_NONE;
      break;
    case SLEEP:
      state_led = LED_OFF;
      break;
    default:
      state_led = LED_OFF;
      break;
    }

    // Debug.printf("Current LED state %d\n", state_led);
    auto current_led = ledcRead(LED_CHANNEL);

    // handle LED state
    switch (state_led)
    {
    case LED_OFF:
      ledcWrite(LED_CHANNEL, 0);
      break;
    case LED_ON:
      ledcWrite(LED_CHANNEL, LED_MAX);
      break;
    case LED_FADING:
      if (current_led > LED_MAX)
      {
        led_step = -LED_STEP;
      }
      else if (current_led <= 0)
      {
        led_step = LED_STEP;
      }
      ledcWrite(LED_CHANNEL, current_led + led_step);
      break;
    case LED_FLASHING:
      if (millis() - last > 100)
      {
        ledcWrite(LED_CHANNEL, ledcRead(0) > LED_MIN ? LED_MIN : LED_MAX);
        last = millis();
      }
      break;
    case LED_HEARTRATE:
      ledcWrite(LED_CHANNEL, heartrate_map * LED_MAX);
      break;
    default:
      break;
    }
    delay(10);

    if (state_current == SLEEP)
    {
      ledcWrite(LED_CHANNEL, 0);
      ledcDetachPin(LED_PIN);
      digitalWrite(LED_PIN, LOW);
      break;
    }
  }

  vTaskDelete(NULL);
}

void setupOTA()
{
  // Port defaults to 3232
  // ArduinoOTA.setPort(3232);

  // Hostname defaults to esp3232-[MAC]
  ArduinoOTA.setHostname(sensor_name.c_str());

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]()
                     {
                       String type;
                       if (ArduinoOTA.getCommand() == U_FLASH)
                         type = "sketch";
                       else // U_SPIFFS
                         type = "filesystem";

                       // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                       Serial.println("Start updating " + type);
                       state_current = UPDATING;
                     })
      .onEnd([]()
             {
               Serial.println("\nEnd");
               delay(500);
             })
      .onProgress([](unsigned int progress, unsigned int total)
                  {
                    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
                    Debug.printf("Progress: %u%%\n", (progress / (total / 100)));
                    Debug.printf("Progress LED: %f%%\n", (uint32_t)((1024.f / total) * progress));
                    Debug.handle();
                    ledcWrite(LED_CHANNEL, (1024.f / total) * progress);
                  })
      .onError([](ota_error_t error)
               {
                 Serial.printf("Error[%u]: ", error);
                 Debug.printf("Error[%u]: ", error);
                 if (error == OTA_AUTH_ERROR)
                   Serial.println("Auth Failed");
                 else if (error == OTA_BEGIN_ERROR)
                   Serial.println("Begin Failed");
                 else if (error == OTA_CONNECT_ERROR)
                   Serial.println("Connect Failed");
                 else if (error == OTA_RECEIVE_ERROR)
                   Serial.println("Receive Failed");
                 else if (error == OTA_END_ERROR)
                   Serial.println("End Failed");
               });

  ArduinoOTA.begin();
}

void setupADC()
{
  esp_adc_cal_characteristics_t adc_chars;
  esp_adc_cal_value_t val_type = esp_adc_cal_characterize((adc_unit_t)ADC_UNIT_1, (adc_atten_t)ADC1_CHANNEL_6, (adc_bits_width_t)ADC_WIDTH_BIT_12, 1100, &adc_chars);
  //Check type of calibration value used to characterize ADC
  if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF)
  {
    Serial.printf("eFuse Vref:%u mV", adc_chars.vref);
    vref = adc_chars.vref;
  }
  else if (val_type == ESP_ADC_CAL_VAL_EFUSE_TP)
  {
    Serial.printf("Two Point --> coeff_a:%umV coeff_b:%umV\n", adc_chars.coeff_a, adc_chars.coeff_b);
  }
  else
  {
    Serial.println("Default Vref: 1100mV");
  }
}

void setupRTC()
{
  rtc.begin(Wire);
  //Check if the RTC clock matches, if not, use compile time
  rtc.check();

  RTC_Date datetime = rtc.getDateTime();
  hh = datetime.hour;
  mm = datetime.minute;
  ss = datetime.second;
}

float getVoltage()
{
  uint16_t v = analogRead(BATT_ADC_PIN);
  return ((float)v / 4095.0) * 2.0 * 3.3 * (vref / 1000.0);
}

float min(RingBuf<uint32_t, BUFSIZE> &buf)
{
  uint32_t m = 3200000000;
  for (size_t i = 0; i < buf.size(); i++)
  {
    if (buf[i] < m)
    {
      m = buf[i];
    }
  }
  return m;
}

void startDeepsleep()
{
  // set state to turn LED off and so on
  state_current = SLEEP;
  // gpio_hold_en(GPIO_NUM_4);
  // gpio_deep_sleep_hold_en();

  // stop heartsensor
  heartSensor.setPulseAmplitudeRed(0);
  heartSensor.setPulseAmplitudeGreen(0);
  heartSensor.setPulseAmplitudeIR(0);
  heartSensor.shutDown();

  // send IMU to sleep
  IMU.setSleepEnabled(true);

  // wait a bit and go to sleep
  delay(500);
  // wakeup on button press
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_33, HIGH);
  delay(500);
  esp_deep_sleep_start();
}

float max(RingBuf<uint32_t, BUFSIZE> &buf)
{
  uint32_t m = 0;
  for (size_t i = 0; i < buf.size(); i++)
  {
    if (buf[i] > m)
    {
      m = buf[i];
    }
  }
  return m;
}