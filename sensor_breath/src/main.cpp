#include <Arduino.h>
#include <ArduinoJson.h>
#include <Filters.h>
#include <RingBuf.h>
#include <FS.h> //this needs to be first, or it all crashes and burns...
#if defined(ESP8266)
#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino
#else
#include <WiFi.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h>
#include <ArduinoOSCWiFi.h>

#define SENSOR_WIND 33
#define SENSOR_TEMP 35
#define LED 2

#define DELAY 50
// #define FREQ_AVG_COUNT 4
// #define MAP_BUFSIZE 4
#define EXHALE_WINDOW 4
#define WIND_BUFSIZE 32
#define LOWPASS_FREQ (1000. / DELAY)
#define WINDOW 20.0 / LOWPASS_FREQ
// adjust if exhale is not detected or too noisy
#define EXHALE_DIFFERENCE 250

// inhalation and frequqncy
uint8_t exhaling = false;
// long last_inhalation = millis();
// float inhalation_freq = 0.0; // in breaths per minute

RingBuf<float, WIND_BUFSIZE> buf_wind;

FilterOnePole windLowPass(LOWPASS, LOWPASS_FREQ);
FilterOnePole exhaleLowPass(LOWPASS, 1.);
FilterOnePole tempLowPass(LOWPASS, LOWPASS_FREQ);
RunningStatistics filterWindOneLowpassStats; // create running statistics to smooth these values
RunningStatistics filterExhaleOneLowpassStats; // create running statistics to smooth these values
RunningStatistics filterTempOneLowpassStats; // create running statistics to smooth these values

// values
float wind_raw = 0;
float wind_mean = 0;
float wind_min = 0;
float wind_max = 0;
float wind_variance = 0;
float wind_map = 0;
float wind_derivative = 0;

float temp_mean = 0;

uint8_t rssi = 0;

// JSON
StaticJsonDocument<300> jsonBuffer;

// WiFI stuff
AsyncWebServer server(80);
DNSServer dns;
AsyncWiFiManager wifiManager(&server, &dns);

// OSC
int osc_port = 5460;
// char *osc_port_str = "54445";
String osc_host = "192.168.100.255";

// AsyncWiFiManagerParameter custom_host("OSC", "osc_host", osc_host, 40);
// AsyncWiFiManagerParameter custom_port("OSC", "osc_port", osc_port_str, 6);

// declarations
float max_buf(RingBuf<float, WIND_BUFSIZE> *b);
float min_buf(RingBuf<float, WIND_BUFSIZE> *b);
// float mean_buf(RingBuf<float, WIND_BUFSIZE> *b);

void setup()
{
  // setup serial
  Serial.begin(115200);

  // setup GPIO
  // pinMode(SENSOR_WIND, INPUT);
  // pinMode(SENSOR_TEMP, INPUT);
  // analogReadResolution(10);
  ledcSetup(0, 5000, 10);
  ledcAttachPin(LED, 0);

  filterWindOneLowpassStats.setWindowSecs(WINDOW);
  filterExhaleOneLowpassStats.setWindowSecs(WINDOW);
  filterTempOneLowpassStats.setWindowSecs(WINDOW);

  // setup WiFi
  // AsyncWiFiManagerParameter custom_bind_port("OSC", "osc_bind_port", osc_bind_port, 5);
  // AsyncWiFiManagerParameter custom_send_port("OSC", "osc_send_port", osc_send_port, 5);

  // AsyncWiFiManager wifiManager(&server, &dns);
  // wifiManager.addParameter(&custom_bind_port);
  // wifiManager.addParameter(&custom_send_port);
  // wifiManager.addParameter(&custom_host);
  // wifiManager.addParameter(&custom_port);

  if (!wifiManager.autoConnect("breath-sensor"))
  {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
  }

  Serial.println(WiFi.localIP());
  Serial.println(WiFi.broadcastIP());
  // set OSC host to broadcast
  osc_host = WiFi.broadcastIP().toString();

  // OscWiFi.publish(osc_host, osc_publish_port, "/breath/wind/min", wind_min)->setFrameRate(20.f);
  // OscWiFi.publish(osc_host, osc_publish_port, "/breath/wind/max", wind_max)->setFrameRate(20.f);
  OscWiFi.publish(osc_host, osc_port, "/breath/raw", wind_raw)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, "/breath/mean", wind_mean)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, "/breath/map", wind_map)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, "/breath/variance", wind_variance)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, "/breath/exhale", exhaling)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, "/breath/temperature", temp_mean)->setFrameRate(10.f);
  OscWiFi.publish(osc_host, osc_port, "/breath/rssi", rssi)->setFrameRate(5.f);
}

void loop()
{
  // get data
  wind_raw = analogRead(SENSOR_WIND);
  auto temp = analogRead(SENSOR_TEMP);
  // float wind_mph = pow((((float)wind - 264.0) / 85.6814), 3.36814); // wind formula derived from a wind tunnel data, annemometer and some fancy Excel regressions
  // Serial.printf("Raw: %04d %04d %.2f mph\n", wind, temp, wind_mph);

  // temp
  tempLowPass.input(temp);
  filterTempOneLowpassStats.input(tempLowPass.output());
  temp_mean = filterTempOneLowpassStats.mean();

  // wind
  windLowPass.input(wind_raw);
  filterWindOneLowpassStats.input(windLowPass.output());
  wind_mean = filterWindOneLowpassStats.mean();

  exhaleLowPass.input(wind_mean);
  filterExhaleOneLowpassStats.input(exhaleLowPass.output());
  auto exhale_mean = filterExhaleOneLowpassStats.mean();
  exhaling = wind_mean > exhale_mean + EXHALE_DIFFERENCE;

  // fill ringbuffer
  if (buf_wind.isFull())
  {
    float tmp;
    buf_wind.pop(tmp);
  }
  buf_wind.push(wind_mean);

  // get min, max
  wind_min = min_buf(&buf_wind);
  wind_max = max_buf(&buf_wind);
  wind_variance = wind_max-wind_min;

  // map [0..1]
  wind_map = wind_mean - wind_min;
  wind_map *= 1.0 / (wind_max - wind_min);
  // wind_map = wind_mean / 4096.;

  // inhalation = wind_map > 0.5f;

  // print data
  // Serial.print(1023-raw);
  jsonBuffer["min"] = wind_min;
  jsonBuffer["max"] = wind_max;
  jsonBuffer["variance"] = wind_variance;
  // jsonBuffer["temp"] = temp; //((((float)temp * 5.0) / 1024.0) - 0.400) / .0195; // temperature in C
  jsonBuffer["temp"] = temp_mean;
  // jsonBuffer["wind"] = wind_mph;
  jsonBuffer["wind"] = wind_mean;
  //sensor["wind_kalman"] = kstate.x;
  //  sensor["wind_lp"] = windLowPass.output();
  //  sensor["wind_mean"] = filterOneLowpassStats.mean();
  //  sensor["wind_sigma"] = filterOneLowpassStats.sigma();
  //  sensor["wind_var"] = filterOneLowpassStats.variance();
  jsonBuffer["map"] = wind_map;
  jsonBuffer["exhale"] = exhaling;
  //sensor["freq"] = inhalation_freq;

  serializeJson(jsonBuffer, Serial);
  Serial.println();

  // set LED state
  //analogWrite(LED, wind_map*255.0);
  ledcWrite(LED, wind_map * 1024);

  // update rssi
  rssi = WiFi.RSSI();

  // update OSC
  OscWiFi.update();

  delay(DELAY);
}

float max_buf(RingBuf<float, WIND_BUFSIZE> *b)
{
  float m = -1000.0f;
  for (size_t i = 0; i < b->size(); i++)
  {
    if ((*b)[i] > m)
    {
      m = (*b)[i];
    }
  }
  return m;
}

float min_buf(RingBuf<float, WIND_BUFSIZE> *b)
{
  float m = 1000.0f;
  for (size_t i = 0; i < b->size(); i++)
  {
    if ((*b)[i] < m)
    {
      m = (*b)[i];
    }
  }
  return m;
}

// float mean_buf(RingBuf<float, MAP_BUFSIZE> *b)
// {
//   if (!b->size())
//     return 0.0f;
//   float m = 0.0f;
//   for (size_t i = 0; i < b->size(); i++)
//   {
//     m += (*b)[i];
//   }
//   return m / b->size();
// }